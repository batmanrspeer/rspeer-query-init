package api.utils;

import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.Projection;

import java.awt.*;

public class DistanceUtils {

    public static double dRadius(double x1, double y1, double x2, double y2) {
        double x0=x2-x1, y0=y2-y1;
        return (x0*x0)+(y0*y0);
    }

    public static double dRadius(double x2, double y2) {
        Point mapPosition = Projection.toMinimap(Players.getLocal().getPosition());
        double x1=mapPosition.getX(), y1=mapPosition.getY();
        double x0=x2-x1, y0=y2-y1;
        return (x0*x0)+(y0*y0);
    }

    public static double getMapTileDistance(double radius) {
        Position playerPoint = Players.getLocal().getPosition();
        Point modifiedPoint = new Point(playerPoint.getX(), playerPoint.getY());
        return DistanceUtils.dRadius(playerPoint.getX(), playerPoint.getY(), modifiedPoint.getX()+radius, modifiedPoint.getY());
    }

}
