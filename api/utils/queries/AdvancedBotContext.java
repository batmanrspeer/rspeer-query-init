package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import api.utils.queries.base.Functionality;
import api.utils.thread.ScriptRunnable;
import org.rspeer.script.Script;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.function.Consumer;
import java.util.function.Function;

public class AdvancedBotContext {

    private final Script script;
    private final Functionality functionality;
    private final PlayerQuery players;
    private final PickableQuery pickables;
    private final NpcQuery npcs;
    private final InventoryQuery inventory;
    private final SceneObjectsQuery gameobjects;
    private final Background background;

    public AdvancedBotContext(Script script) {
        this.script = script;
        functionality = new Functionality();
        players = new PlayerQuery();
        pickables = new PickableQuery();
        npcs = new NpcQuery();
        inventory = new InventoryQuery();
        gameobjects = new SceneObjectsQuery();
        background = new Background(this);
    }

    public Script script() {
        return script;
    }

    public Functionality functionality() {
        return functionality;
    }

    public Background background() {
        return background;
    }

    public PlayerQuery players(Object... settings) {
        settings(players, settings);
        return players;
    }

    public NpcQuery npcs(Object... settings) {
        settings(npcs, settings);
        return npcs;
    }

    public InventoryQuery inventory(Object... settings) {
        settings(inventory, settings);
        return inventory;
    }

    public SceneObjectsQuery gameobjects(Object... settings) {
        settings(gameobjects, settings);
        return gameobjects;
    }

    public PickableQuery grounditems(Object... settings) {
        settings(gameobjects, settings);
        return pickables;
    }

    public void settings(AbstractQuery query, Object... settings) {
        if(settings.length < 1) {
            query.refresh();
        } else {
            try {
                if ((Boolean) settings[0]) {
                    query.refresh();
                }
            } catch (ClassCastException e) {
                functionality().printMessage(0, "PlayerQuery", "AdvancedBotContext", "Invalid setting at at 0. Should be Boolean.");
            }
        }
    }

    public <T> boolean safeOperation(T toCheck, Function<T, Boolean> operation) {
        if(toCheck == null || operation == null) {
            return false;
        }
        return operation.apply(toCheck);
    }

    public boolean safeOperation(Runnable toRun) {
        return safeOperation(toRun, (Consumer<Exception>) Throwable::printStackTrace);
    }

    public boolean safeOperation(Runnable toRun, Consumer<Exception> exceptionHandler) {
        try {
            toRun.run();
            return true;
        } catch (Exception e) {
            try {
                exceptionHandler.accept(e);
            } catch(Exception superUnhandled) {
                e.printStackTrace();
                superUnhandled.printStackTrace();
            }
        }
        return false;
    }



}
