package api.utils.queries;

import api.utils.queries.base.abstracts.BackgroundTask;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Every task in this toRun
 */

public class Background {

    private final ThreadPoolExecutor light, heavy;
    private final AdvancedBotContext ctx;

    public Background(AdvancedBotContext ctx) {
        this.ctx = ctx;
        light = (ThreadPoolExecutor) Executors.newFixedThreadPool(2);
        heavy = (ThreadPoolExecutor)Executors.newCachedThreadPool();
    }

    /**
     * Similar to async, for tasks that do not have
     * heavy computation and will not loop.
     *
     * @param toRun
     */
    public void light(Runnable toRun) {
        light.execute(new BackgroundTask(ctx, toRun));
    }

    /**
     * Creates a new thread for the given task and will loop this task till the script stops.
     *
     * @param toRun a block to be run on a unique thread.
     */
    public void heavy(Runnable toRun) {
        heavy.execute(new BackgroundTask(ctx, toRun));
    }

    public void heavy(Runnable toRun, long timeToSleep) {
        heavy.execute(new BackgroundTask(ctx, toRun, timeToSleep));
    }

    public void heavy(Runnable toRun, Callable<Boolean> optionalStoppingCondition, long timeToSleep) {
        heavy.execute(new BackgroundTask(ctx, toRun, optionalStoppingCondition, timeToSleep));
    }

}
