package api.utils.queries;

import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.Bank;

import java.util.ArrayList;

public class BankQuery extends ItemBasedQuery<BankQuery, Item> {

    @Override
    protected BankQuery getI() {
        return this;
    }

    @Override
    public ArrayList<Item> refreshQueryData() {
        return convertArrayToList(Bank.getItems());
    }

}
