package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import org.rspeer.runetek.adapter.component.InterfaceComponent;
import org.rspeer.runetek.api.component.Interfaces;

import java.util.ArrayList;

public class InterfaceQuery extends AbstractQuery<InterfaceQuery, InterfaceComponent[]> {
    @Override
    protected InterfaceQuery getI() {
        return this;
    }

    @Override
    protected ArrayList<InterfaceComponent[]> refreshQueryData() {
        return convertArrayToList(Interfaces.getAll());
    }

}
