package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import api.utils.queries.base.QueryDelegate;
import api.utils.queries.base.interfaces.QueryIdentifiable;
import api.utils.queries.base.interfaces.QueryInteractable;
import org.rspeer.runetek.adapter.component.Item;
import org.rspeer.runetek.api.component.tab.Inventory;

import java.util.ArrayList;

public class InventoryQuery extends AbstractQuery<InventoryQuery, Item> implements QueryIdentifiable<InventoryQuery, Item>, QueryInteractable<InventoryQuery, Item> {

    public InventoryQuery getAt(Integer... index) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::getIndex, index));
    }

    public InventoryQuery stacksize(Integer... size) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::getStackSize, size));
    }

    public InventoryQuery stacksable(Boolean condition) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::isStackable, condition));
    }

    public InventoryQuery noteable(Boolean condition) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::isStackable, condition));
    }

    public InventoryQuery exchangeable(Boolean condition) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::isExchangeable, condition));
    }

    public Item itemAt(int itemAt) {
        return Inventory.getItemAt(itemAt);
    }

    public Item selectedItem() {
        return Inventory.getSelectedItem();
    }

    public boolean isFull() {
        return Inventory.isFull();
    }

    public int count() {
        return Inventory.getCount();
    }

    @Override
    protected InventoryQuery getI() {
        return this;
    }

    @Override
    public ArrayList<Item> refreshQueryData() {
        return convertArrayToList(Inventory.getItems());
    }

}
