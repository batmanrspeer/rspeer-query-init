package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import api.utils.queries.base.QueryDelegate;
import api.utils.queries.base.interfaces.QueryIdentifiable;
import api.utils.queries.base.interfaces.QueryInteractable;
import org.rspeer.runetek.adapter.component.Item;

public abstract class ItemBasedQuery<I extends ItemBasedQuery, T extends Item>
        extends AbstractQuery<I, T> implements QueryIdentifiable<I, T>, QueryInteractable<I, T> {

    public I getAt(Integer... index) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::getIndex, index));
    }

    public I stacksize(Integer... size) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::getStackSize, size));
    }

    public I stacksable(Boolean condition) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::isStackable, condition));
    }

    public I noteable(Boolean condition) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::isStackable, condition));
    }

    public I exchangeable(Boolean condition) {
        return queryData(QueryDelegate.search(negate(), queryData(), Item::isExchangeable, condition));
    }


}
