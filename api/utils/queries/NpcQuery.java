package api.utils.queries;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.api.scene.Npcs;

import java.util.ArrayList;

public class NpcQuery extends PathingEntityQuery<NpcQuery, Npc> {

    @Override
    protected NpcQuery getI() {
        return this;
    }


    @Override
    public ArrayList<Npc> refreshQueryData() {
        return convertArrayToList(Npcs.getLoaded());
    }

}
