package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import api.utils.queries.base.interfaces.QueryRSPathingEntity;
import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.Positionable;
import org.rspeer.runetek.api.commons.Identifiable;
import org.rspeer.runetek.providers.RSPathingEntity;

public abstract class PathingEntityQuery<I extends PathingEntityQuery, T extends RSPathingEntity & Positionable & Identifiable & Interactable>
        extends AbstractQuery<I, T>
        implements
        QueryRSPathingEntity<I, T> {

}
