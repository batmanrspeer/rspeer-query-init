package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import api.utils.queries.base.QueryDelegate;
import api.utils.queries.base.interfaces.QueryIdentifiable;
import api.utils.queries.base.interfaces.QueryInteractable;
import api.utils.queries.base.interfaces.QueryPositionable;
import org.rspeer.runetek.adapter.scene.Pickable;
import org.rspeer.runetek.api.scene.Pickables;

import java.util.ArrayList;

public class PickableQuery extends AbstractQuery<PickableQuery, Pickable>
        implements QueryIdentifiable<PickableQuery, Pickable>, QueryInteractable<PickableQuery, Pickable>, QueryPositionable<PickableQuery, Pickable> {

    public PickableQuery stacksize(Integer... size) {
        return queryData(QueryDelegate.search(negate(), queryData(), Pickable::getStackSize, size));
    }

    @Override
    protected PickableQuery getI() {
        return this;
    }

    @Override
    public ArrayList<Pickable> refreshQueryData() {
        return convertArrayToList(Pickables.getLoaded());
    }
}
