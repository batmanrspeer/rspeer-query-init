package api.utils.queries;

import api.utils.queries.base.QueryDelegate;
import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.api.scene.Players;

import java.util.ArrayList;
import java.util.Arrays;


public class PlayerQuery extends PathingEntityQuery<PlayerQuery, Player> {

    public PlayerQuery skullIcons(Integer... required) {
        queryData(QueryDelegate.search(negate(), data(), Player::getSkullIcon, required));
        return this;
    }

    public PlayerQuery getTeam(Integer... required) {
        queryData(QueryDelegate.search(negate(), data(), Player::getTeam, required));
        return this;
    }

    public PlayerQuery prayerIcons(Integer... required) {
        queryData(QueryDelegate.search(negate(), data(), Player::getPrayerIcon, required));
        return this;
    }

    public Player local() {
        return Players.getLocal();
    }

    @Override
    public ArrayList<Player> refreshQueryData() {
        return new ArrayList<>(Arrays.asList(Players.getLoaded()));
    }

    @Override
    protected PlayerQuery getI() {
        return this;
    }
}
