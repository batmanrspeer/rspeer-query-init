package api.utils.queries;

import api.utils.queries.base.AbstractQuery;
import api.utils.queries.base.QueryDelegate;
import api.utils.queries.base.interfaces.QueryIdentifiable;
import api.utils.queries.base.interfaces.QueryInteractable;
import api.utils.queries.base.interfaces.QueryPositionable;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.scene.SceneObjects;

import java.util.ArrayList;

public class SceneObjectsQuery extends AbstractQuery<SceneObjectsQuery, SceneObject>
        implements QueryIdentifiable<SceneObjectsQuery, SceneObject>, QueryPositionable<SceneObjectsQuery, SceneObject>, QueryInteractable<SceneObjectsQuery, SceneObject> {

    public SceneObjectsQuery orientations(Integer... required) {
        return queryData(QueryDelegate.search(negate(), data(), SceneObject::getOrientation, required));
    }

    @Override
    protected SceneObjectsQuery getI() {
        return this;
    }

    @Override
    public ArrayList<SceneObject> refreshQueryData() {
        return convertArrayToList(SceneObjects.getLoaded());
    }

}
