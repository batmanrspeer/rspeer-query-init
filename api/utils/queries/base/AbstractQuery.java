package api.utils.queries.base;

import api.utils.ArrayUtils;
import api.utils.queries.base.interfaces.Queryable;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 * All-Query, if you know what I mean. ;)
 *
 * @param <T> is the type of the query
 */
public abstract class AbstractQuery<I extends Queryable, T> implements Queryable<I, T> {

    private final ThreadLocal<List<T>> queryData;

    private final ThreadLocal<AtomicBoolean> isNegation;

    public AbstractQuery() {
        queryData = ThreadLocal.withInitial(CopyOnWriteArrayList::new);
        isNegation = ThreadLocal.withInitial(AtomicBoolean::new);
    }

    protected abstract I getI();

    protected abstract ArrayList<T> refreshQueryData();

    protected List<T> data() {
        return queryData.get();
    }

    public ArrayList<T> queryData() {
        return new ArrayList<>(data());
    }

    @Override
    public I refresh() {
        queryData(refreshQueryData());
        return getI();
    }

    @Override
    public I limit(int maxNumberOfItems) {
        return limit(0, maxNumberOfItems - 1);
    }

    @Override
    public I limit(int lowerLimit, int upperLimit) {
        ArrayList<T> newQueryData = new ArrayList<T>();
        for (int i = lowerLimit; i < queryData.get().size() && i <= upperLimit; i++) {
            newQueryData.add(queryData.get().get(i));
        }
        queryData(newQueryData);
        return getI();
    }

    @Override
    public I sort(Comparator<T> criteria) {
        queryData.get().sort(criteria);
        return getI();
    }

    @Override
    public I shuffle() {
        Collections.shuffle(queryData.get());
        return getI();
    }

    @Override
    public T first() {
        T toReturn = peek();
        if (toReturn != null) {
            queryData.get().remove(0);
        }
        return toReturn;
    }

    public T last() {
        T toReturn;
        try {
            toReturn = queryData.get().get(size()-1);
        } catch(Exception e) {
            toReturn = null;
        }
        return toReturn;
    }

    @Override
    public T peek() {
        try {
            return queryData.get().get(0);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public I reverse() {
        Collections.reverse(queryData.get());
        return getI();
    }

    private static <T> void setArrayList(List<T> a, List<T> b) {
        a.clear();
        a.addAll(b);
    }

    public I queryData(List<T> values) {
        setArrayList(queryData.get(), values);
        return getI();
    }

    protected static <T> ArrayList<T> convertArrayToList(T[] array) {
        return ArrayUtils.convertArrayToList(array);
    }

    @Override
    public boolean negate() {
        return isNegation.get().get();
    }

    @Override
    public void negate(boolean negate) {
        isNegation.get().set(negate);
    }

    @Override
    public boolean contains(T value) {
        return queryData.get().contains(value);
    }

    @Override
    public boolean IsEmpty() {
        return this.queryData.get().isEmpty();
    }

    @Override
    public int size() {
        return this.queryData.get().size();
    }

}
