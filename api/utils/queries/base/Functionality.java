package api.utils.queries.base;

import api.utils.StopWatch;
import api.utils.objects.OptimalObject;

import java.util.HashSet;
import java.util.logging.Logger;

/**
 * This is used for creating Tree Functionality.
 * Be advised that it comes in TreeScript directly.
 * You as a writer do not need to create an instance of this.
 *
 * @author DopeAssFreshPrince
 * @since 2017
 * @version 1.0aaa
 */

public class Functionality {

    private final Logger log;
    private final StopWatch stopWatch;
    private long startTime;
    private static OptimalObject<Integer> debugLevel;
    //TODO SYNCLOCK

    private String lastMessage;

    /**
     * This creates an instance of TreeFunctionality.
     *
     */
    public Functionality() {
        this.stopWatch = new StopWatch();
        this.log = Logger.getLogger("[SCRIPT]:");
        lastMessage = "Nothing yet.";
        debugLevel = new OptimalObject<>(5);
        startTime = 0;
    }

    /**
     * Pause the script's stop watch. Handled by TreeScript.
     */
    public void pauseStopWatch() {
        stopWatch.pause();
    }

    /**
     * Resumes the script's stop watch. Handled by TreeScript.
     */
    public void resumeStopWatch() {
        stopWatch.resume();
    }

    /**
     * Prints a message via log.info and stores it in
     * last message if given debug level is less than or equal to set
     * {@link this#debugLevel}.
     *
     * @param debugLevel is the debug level of the source.
     * @param source     is the source of the message.
     * @param type       is the type of message's source.
     * @param message    is the actual message.
     * @return returns true if the message was accepted.
     */
    public boolean printMessage(final int debugLevel, final String source, final String type, final String message) {
        if(debugLevel <= this.debugLevel.get()) {
            final long timeStamp = getRunTime() / 1000;
            final String timeStampAsString = getRuntimeAsString(timeStamp);
            lastMessage = ("[" + timeStampAsString + "][" + source + "|" + type + "]:" + message);
            log.info(lastMessage);
            return true;
        }
        return false;
    }

    /**
     * Gives the last successful message.
     *
     * @return the last successful message.
     */
    public String getLastMessage() {
        return lastMessage;
    }

    /**
     * Gives the runtime of the script.
     *
     * @return the run time of the script.
     */
    public long getRunTime() {
        return stopWatch.getTimeInMilliSeconds();
    }

    /**
     * Gives the runtime of the script, including the time it was paused.
     *
     * @return the total runtime.
     */
    public long getTotalRunTime() {
        return stopWatch.getTotalTimeInMilliSeconds();
    }

    //Gives the set debug level.

    /**
     * Allows the setting the debug level of the script.
     *
     * @param debugLevel to store in {@link this#debugLevel}.
     */
    public void setDebugLevel(final int debugLevel) {
        this.debugLevel.set(debugLevel);
    }

    /**
     * Gives the runtime excluding the pause time as a string.
     *
     * @return the runtime as a formatted string.
     */
    public final String getRuntimeAsString() {
        return getRuntimeAsString(stopWatch.getTimeInMilliSeconds()/1000);
    }

    /**
     * Gives the total time as a string.
     *
     * @return the total runtime as a formatted string.
     */
    public final String getTotalRuntimeAsString() {
        return getRuntimeAsString(stopWatch.getTotalTimeInMilliSeconds()/1000);
    }

    /**
     * Gives formatted time as a string where you can specify a custom timestamp.
     *
     * @param seconds the timestamp in seconds.
     *
     * @return the runtime as a string.
     */
    public final String getRuntimeAsString(final long seconds) {
        final long second = (seconds) % 60;
        final long minute = (seconds / 60) % 60;
        final long hour = (seconds / 3600) % 24;
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }

    /**
     * Calculates the number of x one may achieve in an hour.
     * The function assumes the value has been obtained within script time.
     *
     * @param x is the value to calculate for.
     *
     * @return the amount x obtained per hour.
     */
    public long XInAnHour(long x) {
        return XInAnHour(x, getRunTime());
    }

    /**
     * The base for x in an hour calculation.
     *
     * @param x is the value to calculate for.
     * @param timePassed is the time x was obtained in.
     * @return the amount x obtained per hour.
     */
    public long XInAnHour(long x, long timePassed) {
        return ((60 * 60 * 1000) * x) / timePassed;
    }


}
