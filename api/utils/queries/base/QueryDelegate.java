package api.utils.queries.base;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QueryDelegate {

    public static <L, R> Stream<L> toUse(List<L> list, List<R> requiredList, int cFactor) {
        if(list.size()*requiredList.size()*cFactor > 10000) {
            return list.parallelStream();
        }
        return list.stream();
    }

    @SafeVarargs
    public static <L, R> List<L> search(boolean negate, List<L> list, Function<L, R> query, int cFactor, R... required)  {
        List<R> requiredList = new ArrayList<>();
        Collections.addAll(requiredList, required);
        return toUse(list, requiredList, cFactor).filter((i) -> {
            boolean toReturn = requiredList.contains(query.apply(i));
            if(negate) {
                toReturn = !toReturn;
            }
            return  toReturn;
        }).collect(Collectors.toList());
    }

    @SafeVarargs
    public static <L, R> List<L> search(boolean negate, List<L> list, Function<L, R> query, R... required)  {
        return search(negate, list, query, 1, required);
    }

    @SafeVarargs
    public static <L, R> List<L> search(List<L> list, Function<L, R> query, R... required)  {
        return search(false, list, query, 1, required);
    }

    @SafeVarargs
    public static <T, I> List<T> booleanSearch(boolean negate, List<T> list, BiFunction<T, List<I>, Boolean> query, int cFactor, I... required) {
        ArrayList<I> requiredList = new ArrayList<>(required.length);
        Collections.addAll(requiredList, required);
        return toUse(list, requiredList, cFactor).filter((i)-> negate? !query.apply(i, requiredList):query.apply(i, requiredList)).collect(Collectors.toList());
    }

}
