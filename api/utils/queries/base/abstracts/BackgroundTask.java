package api.utils.queries.base.abstracts;

import api.utils.queries.AdvancedBotContext;

import java.util.concurrent.Callable;

public class BackgroundTask implements Runnable {

    private Runnable toRun;
    private AdvancedBotContext ctx;
    private long sleepTime = 250;
    private Callable<Boolean> toStop = ()->false;

    public BackgroundTask(AdvancedBotContext ctx, Runnable toRun) {
        this.ctx = ctx;
        this.toRun = toRun;
    }

    public BackgroundTask(AdvancedBotContext ctx, Runnable toRun, long sleepTime) {
        this(ctx, toRun);
    }

    public BackgroundTask(AdvancedBotContext ctx, Runnable toRun, Callable<Boolean> toStop, long sleepTime) {
        this(ctx, toRun, sleepTime);
        this.toStop = toStop;
    }

    @Override
    public final void run() {
        try {
            while (!ctx.script().isStopping() || toStop.call()) {
                while (!ctx.script().isPaused()) {
                    toRun.run();
                }
                try {
                    this.wait(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.out.println("Unhandled Exception raised for a background task");
            e.printStackTrace();
        }
    }

}
