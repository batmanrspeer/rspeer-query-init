package api.utils.queries.base.interfaces;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public interface IAbstractQuery<I extends  IAbstractQuery, T> {

    I refresh();

    I limit(int maxNumberOfItems);

    I limit(int lowerLimit, int upperLimit);

    I sort(Comparator<T> criteria);

    I shuffle();

    T first();

    T peek();

    I reverse();

    boolean negate();

    List<T> queryData();

    I queryData(List<T> values);

    void negate(boolean negate);

    boolean contains(T value);

    boolean IsEmpty();

    int size();
}
