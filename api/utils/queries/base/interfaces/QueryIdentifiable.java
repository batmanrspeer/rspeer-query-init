package api.utils.queries.base.interfaces;

import api.utils.queries.base.QueryDelegate;
import org.rspeer.runetek.api.commons.Identifiable;

public interface QueryIdentifiable<I extends QueryIdentifiable, T extends Identifiable> extends Queryable<I, T> {

    default I ids(Integer... required) {
        return queryData(QueryDelegate.search(negate(), queryData(), Identifiable::getId, required));
    }

    default I names(String... required) {
        return queryData(QueryDelegate.search(negate(), queryData(), Identifiable::getName, required));
    }


}
