package api.utils.queries.base.interfaces;

import api.utils.queries.base.QueryDelegate;
import org.rspeer.runetek.adapter.Interactable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public interface QueryInteractable<I extends QueryInteractable, T extends Interactable> extends Queryable<I, T> {

    default I actions(String... required) {
        return queryData(stringActions(Interactable::getActions, required));
    }

    default I rawActions(String... required) {
        return queryData(stringActions(Interactable::getRawActions, required));
    }

    default List<T> stringActions(Function<T, String[]> throwableActions, String... required) {
        List<T> list = queryData();
        List<String> requiredList = new ArrayList<>();
        Collections.addAll(requiredList, required);
        return QueryDelegate.toUse(list, requiredList, 5)
                .filter((i)-> negate()? !Collections.disjoint(Arrays.asList(throwableActions.apply(i)), requiredList):Collections.disjoint(Arrays.asList(throwableActions.apply(i)), requiredList))
                .collect(Collectors.toList());

    }


}
