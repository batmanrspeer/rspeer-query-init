package api.utils.queries.base.interfaces;

import api.utils.DistanceUtils;
import api.utils.queries.base.QueryDelegate;
import org.rspeer.runetek.adapter.Positionable;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.Projection;

import java.awt.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public interface QueryPositionable<I extends QueryPositionable, T extends Positionable> extends Queryable<I, T> {

    default I nearest() {
        return nearest(Players.getLocal().getPosition());
    }

    default I nearest(Position required) {
        List<T> list = queryData();
        Comparator<T> toCompareWith = Comparator.comparingDouble((o) -> o.distance(required));
        if(negate()) {
            toCompareWith.reversed();
        }
        list.sort(toCompareWith);
        return queryData(list);
    }

    default I radius(double radius) {
        return radius(radius, Players.getLocal().getPosition());
    }

    default I radius(double radius, Position... required) {
        List<T> list = queryData();
        List<Position> requiredList = Arrays.asList(required);
        final double literalRadius = DistanceUtils.getMapTileDistance(radius), radiusDouble = literalRadius*literalRadius;
        return queryData(QueryDelegate.toUse(list, requiredList, 3)
                .filter((p)-> {
                    for(Position position: requiredList) {
                        Point mapPosition = Projection.toMinimap(position);
                        Point anotherMapPosition = Projection.toMinimap(p.getPosition());
                        if(anotherMapPosition == null) {
                            return false;
                        }
                        if(negate()^(DistanceUtils.dRadius(anotherMapPosition.getX(), anotherMapPosition.getY(), mapPosition.getX(), mapPosition.getY()) <=radiusDouble)) {
                            return true;
                        }
                    }
                    return false;
                }).collect(Collectors.toList()));
    }

}
