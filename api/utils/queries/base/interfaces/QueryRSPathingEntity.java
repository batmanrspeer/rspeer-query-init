package api.utils.queries.base.interfaces;

import api.utils.queries.base.QueryDelegate;
import org.rspeer.runetek.adapter.Interactable;
import org.rspeer.runetek.adapter.Positionable;
import org.rspeer.runetek.adapter.scene.PathingEntity;
import org.rspeer.runetek.api.commons.Identifiable;
import org.rspeer.runetek.providers.RSPathingEntity;

import static api.utils.queries.base.QueryDelegate.search;


public interface QueryRSPathingEntity<I extends QueryRSPathingEntity, T extends RSPathingEntity & Positionable & Identifiable & Interactable>
        extends Queryable<I, T>, QueryIdentifiable<I, T>, QueryPositionable<I, T> , QueryInteractable<I, T>{

    default I combatLevel(Integer... required) {
        return queryData(QueryDelegate.search(negate(), queryData(), t -> t.getWrapper().getCombatLevel(), required));
    }

    default I targets(PathingEntity... required) {
        return queryData(QueryDelegate.search(negate(), queryData(), t -> t.getWrapper().getTarget(), required));
    }

    default I animations(Integer... required) {
        return queryData(QueryDelegate.search(negate(), queryData(), RSPathingEntity::getAnimation, required));
    }

    default I walkingStance(Integer... walkingStances) {
        return queryData(search(negate(), queryData(), RSPathingEntity::getWalkingStance, walkingStances));
    }

    default I animation(Integer... animations) {
        return queryData(search(negate(), queryData(), RSPathingEntity::getAnimation, animations));
    }

    default I targetIds(Integer... ids) {
        return queryData(search(negate(), queryData(), RSPathingEntity::getTargetIndex, ids));
    }

}
